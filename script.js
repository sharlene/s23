let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!")
    }
}

console.log(trainer);

console.log("Result of dot notation:")
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level) {

    // properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    this.fainted = false;

    // methods
    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);
        target.health -= this.attack;
        console.log(target.name + "'s health is now reduced to " + target.health);
        checkFainted(target);
    };

    this.faint = function() {
        console.log(this.name + ' fainted');
        this.fainted = true;
    };
}

function checkFainted(pokemon) {
    if (pokemon.health <= 0 && !pokemon.fainted) {
        pokemon.faint();
    }
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewto', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

// tackle loop
while (!pikachu.fainted) {
    geodude.tackle(pikachu);
}

while (!geodude.fainted) {
    mewtwo.tackle(geodude);
}

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);